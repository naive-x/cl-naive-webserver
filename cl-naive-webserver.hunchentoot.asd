(defsystem "cl-naive-webserver.hunchentoot"
  :description ""
  :version "2023.10.7"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-log
               :cl-naive-webserver
               :cl-who
               :hunchentoot)
  :components ((:file "src/hunchentoot/package"     :depends-on ())
               (:file "src/hunchentoot/hunchentoot" :depends-on ("src/hunchentoot/package"))
               (:file "src/hunchentoot/hunchentoot-patch")))

