(defsystem "cl-naive-webserver.tests"
  :description "Tests for cl-naive-webserver."
  :version "2023.11.1"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-tests
               :cl-naive-webserver.hunchentoot.tests)
  :components ())
