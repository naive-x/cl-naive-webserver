(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew (truename (merge-pathnames
                      (make-pathname :name nil :type nil :version nil
                                     :defaults (or *load-truename*
                                                   *compile-file-truename* #P"./"))
	              #P"../../"))
           ql:*local-project-directories*
           :test #'equalp)
  (ql:quickload '(:cl-naive-webserver.hunchentoot
                 :cl-naive-who-ext
                 :drakma)))


(defpackage :naive-examples (:use :cl :cl-naive-webserver))
(in-package :naive-examples)

(defparameter *server*
  (make-instance 'cl-naive-webserver.hunchentoot:hunchentoot-server
		 :address "localhost"
		 :port 3333))
(start-server *server*)

(defparameter *simple-site*
  (make-instance 'site :url "/simple"))
(register-site *simple-site*)


(setf (find-resource (handlers *simple-site*) "/example")
      (lambda (script-name)
	(declare (ignore script-name))
	(cl-naive-who-ext:with-html-to-string
	  "Yeeeha she is a live and kicking!!")))

(setf (find-resource (handlers *simple-site*) "/get-headers")
      (lambda (script-name)
	(declare (ignore script-name))
        (push (cons :content-type "text/plain") (headers *reply*))
        (push (cons :x-test "Hello, World!") (headers *reply*))
        "Yeeeha she is a live and kicking!!"))

(assert (string= (cdr (assoc :x-test (nth-value 2 (drakma:http-request
                                                   "http://localhost:3333/simple/get-headers"
                                                   :method :get))))
                 "Hello, World!"))

(setf *no-session-p* nil)
(drakma:http-request
 "http://localhost:3333/simple/example"
 :method :get)

(setf *no-session-p* t)
(drakma:http-request
 "http://localhost:3333/simple/example"
 :method :get)

;;Try the following in your browser it should give you an authorization error

;;http://localhost:3333/simple/example

;;Get a token issued first

;;http://localhost:3333/simple/token

;;No try /example again

;;http://localhost:3333/simple/example

