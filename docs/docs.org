# -*- mode:org;coding:utf-8 -*-

#+AUTHOR: Phil Marneweck
#+EMAIL: phil@mts.co.za
#+DATE: Tue Aug 16 15:42:26 CEST 2022

#+TITLE: User Manual for the cl-naive-webserver library

#+BEGIN_EXPORT latex
\clearpage
#+END_EXPORT

* Prologue                                                         :noexport:

#+LATEX_HEADER: \usepackage[english]{babel}
#+LATEX_HEADER: \usepackage[autolanguage]{numprint} % Must be loaded *after* babel.
#+LATEX_HEADER: \usepackage{rotating}
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage[margin=0.75in]{geometry}

# LATEX_HEADER: \usepackage{indentfirst}
# LATEX_HEADER: \setlength{\parindent}{0pt}
#+LATEX_HEADER: \usepackage{parskip}

#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usetikzlibrary{positioning, fit, calc, shapes, arrows}
#+LATEX_HEADER: \usepackage[underline=false]{pgf-umlsd}
#+LATEX_HEADER: \usepackage{lastpage}
#+LATEX_HEADER: \pagestyle{fancyplain}
#+LATEX_HEADER: \pagenumbering{arabic}
#+LATEX_HEADER: \lhead{\small{cl-naive-webserver}}
#+LATEX_HEADER: \chead{}
#+LATEX_HEADER: \rhead{\small{User Manual}}
#+LATEX_HEADER: \lfoot{}
#+LATEX_HEADER: \cfoot{\tiny{\copyright{2021 - 2022 Phil Marneweck}}}
#+LATEX_HEADER: \rfoot{\small{Page \thepage \hspace{1pt} de \pageref{LastPage}}}

* cl-naive-webserver
** Introduction

A site centric web server that uses other HTTP servers as back ends
and exposes its own flavour of what is deemed essential functionality
for a webserver.  Currently hunchentoot is supported out of the box.
Support for woo is a work in progress.

cl-naive-webserver has a sizable API but for day to day use only a
small portion is use.  The API is big because everything in
cl-naive-webserver is customisable!

** API

*** [special] =*sites*=

Where sites are registered.

*** [class] =site= ()

**** [slot] =site=

The external url to the site.

**** [slot] =sessions=

Active sessions for the site. Timed out sessions are removed."

**** [slot] =sessions-p=

Indicate if sessions should be enabled for the site.

**** [slot] =session-timeout=

Session timeout in milliseconds.

**** [slot] =session-class=

Class used to instanciate session objects.

**** [slot] =credentials-class=

Class used to instanciate credentials objects.

**** [slot] =handlers=

Handlers for the site. Handlers are lookedup and called from
handle-request.

**** [slot] =handling-exclusions=

Because lisp webservers are usually run behind a proxy like nginx we
exclude the handling of the normal list of suspects (".js" ".css"
".jpg" ".png" ".gif" ".ico" ".svg" ".woff" ".woff2" ".ttf"
"file-upload") that would be served by the proxy and not the lisp
server.

If you don't want any exclusions just set this to nil or add your own
exclusions.

*** [generic function] =register-site= (=site=)

Stores site for retrieval later.  The default implementation stores it
in =*sites*=.

*** [generic function] [get-site[ (url &optional sites)

Get a registered site.

*** [class] =server= ()

Used to set some basic info needed by HTTP server.

**** [slot] =id=

Any identifier that is unique.

**** [slot] =port=

Port used by server.

**** [slot] =request-class=

Class used to instanciate request objects.

*** [special] =*servers*=

Where servers are registered.

*** [generic function] =register-server= (=server=)

Stores server for retrieval later.  You can have more than one server
and more than one type of server (ie different back end) active at any
one time.

A site can only be served by a single server.  The server uses the URL
of a request on the port it serves to select a site based on its site
URL.  Once it selected a site, it looks for a specific handler in the
sites handlers for the URL and calls that handler if found.

*** [generic function] =get-server= (id &optional server)

Get a registered server.

*** [generic function] =start-server= (server &key &allow-other-keys)

Starts a HTTP server to process requests.

*** [generic function] =stop-server= (server &key &allow-other-keys)

Stops HTTP server processing requests.

*** [class] =request= ()

A sanitized/naive version of what is considered a request object by the HTTP backends supported.

**** [slot] =back-end-request=

Different HTTP server backends have different request objects.  What
ever is considered the request object of the back-end is passed along
in instances of the naive-webserver request object which is a
sanitized version of a typical HTTP request.

If you find your-self going to the back-end-request regularly then you
should extend the request class to cover your scenario and specialize
make-request for each type of back end. So when you do decide to
change back ends that you don't have code every where that needs
changing.  A little work upfront will save you grief later on!  And if
you think your tweak would benefit others log and issue to get the
default request extended.

**** [slot] =headers=

Headers received.

**** [slot] =script-name=

The URI requested by the client without the query string.

**** [slot] =server-protocol=

The HTTP protocol as a keyword.

**** [slot] =remote-address=

The IP address of the client that initiated this request.

**** [slot] =remote-port=

The TCP port number of the client socket from which this request
originated.

**** [slot] =query-string=

The query string of this request.

*** [generic function] =make-request= (server raw-request)

Take the backend request and create a sanitized request.  Needs to be
implemented for each backend.

*** [class] =credentials= ()

This is a class used to store user credentials in. Used during the
authorization process.

The user needs to specialise this class and set the credetials-class
of the site instance to your specialization.

populate-credentials needs to be implemented for the specialization to
populate an instance of this class from a request.

make-credentials should not need specialization but can be
specialized.

*** [class] =credentials-basic= (credentials)

A basic user-name password credentials class.  It is still up to the
user of the package to implement authenticate specialised on
credentials-basic since this package has no concept of what to
authenticate against.

**** [slot] =user-name=

User identifier.

**** [slot] =password=

Password.

*** [generic function] =populate-credentials= (credentials)

Populates credentials from request.  This needs to be specialized if
you are not going to use credentials-basic.

/populate-credentials (credentials)/

Always returns credentials untouched.

/populate-credentials ((credentials credentials-basic))/

Uses user-name and password parameters to populate a credentials-basic
instance.

populate and make-credentials are split this way because the
implementation creates the credentials instance base on the site's
credentials class.

*** [generic function] =make-credentials= (site)

Creates credentials based on credentials-class of the site and calls
populate-credentials to populate the values from the request.

*** [class] =authorization= ()

An authorization object is created during authentication.  The default
class holds the credentials instance and some other basic information
about the authorization. Each session has an authorization instance.

Specialize this if you need some thing more complex.  You will also
have to specialize start-session and authenticate which should return
an authorization instance.

**** [slot] =credentials=

Holds an instance of the credentials used during authorization of then
caller.

**** [slot] =remote-address=

Then callers real IP address.

**** [slot] =time-stamp=

Time stamp of when the authorisation took place..

*** [generic function] =authenticate= (credentials &key &allow-other-keys)

Must check the credentials of caller and return an authorization instance if
authentication passed.

The default implementation ignores the credentials and always returns
a authorization instance.  Credentials and authentication must be
specialised and implemented by the user.

*** [class] =session= ()

To access web resources the caller must first establish a session,
with the correct authentication credentials and then use the token
issued for the session to service further requests.

naive-webserver by passes any session handling done by HTTP backends
prefering its own implementation of sessions. This is because we
implement token based security by default. It also makes it easier to
allow extentions of the session functionality

**** [slot] =site=

Reference to site for convenience.

**** [slot] =authorization=

An object that represents the authorization of the api caller.

**** [slot] =last-click=

Last time the user was active in the session.

**** [slot] =cache=

A place to cache session values. Doing caching on session level, so
that memory can be released when the session dies.

*** [generic function] =issue-token= (site &key authorization)

If authorization passes checks then issue a token for use with further
requests for the session.

The default implementation ignores the authorization and always
returns a token so that sessions can be used without explicit
authentication.  It returns a blake2 MAC using Ironclad, it is complete
overkill because tokens are dumb values and requires no fancy
reversible calculations.

*** [generic function] =start-session= (site credentials &key token &allow-other-keys)

If a session for the token already exists it returns existing the
existing session.

If no session exists for the token it creates a new session based on
the session-class of the site.  It runs authenticate against the
credentials and uses issues-token to issue a token if the authentication
passed.

Start-session should only ever be called from handle-token, which is
called on a explicit token request.

*** [generic function] =get-session= (site token)

Retrieves the session using the token supplied. If the session has
timed out or does not exit no session is returned.

*** [generic function] =handle-token= (site request credentials &key &allow-other-keys)

Handles a request for a token and starts a session. It is called on
URL=/token by default.

It returns the token to caller and sets a cookie \"Auth-Token\" on the
outgoing reply.

*** [generic function] =parameter= (name &key type)

Returns a get or post parameter by name unless type of =:GET= or =:POST=
is supplied to get as specific parameter type.

*** [generic function] =handle-request= (site request &key &allow-other-keys)

The =handle-request= method looks up a function in the handlers slot for
the site and then calls the handler.

By the time the =handle-request= method is called authorization has
already been dealt with.

=*session*= and =*request*= instances are bound for duration of the call
to =handle-request=. This is done in an =:around=.

The default implementation uses the script-name to lookup the handler
to call.

*** [generic function] =bad-request= (request &key &allow-other-keys)

Return HTTP codes and messages to indicate that a request url could
not be handled.

*** [generic function] =bad-request= (request &key &allow-other-keys)

Return HTTP codes and messages to indicate that a request is not
authorized.

*** [class] =cookie= ()

Each cookie object describes one outgoing cookie.

**** [slot] =name=

The name of the cookie - a string.

**** [slot] =value=

The value of the cookie. Will be URL-encoded when sent to the browser.

**** [slot] =expires=

The time (a universal time) when the cookie expires (or NIL).

**** [slot] =max-age=

The time delta (in seconds) after which the cookie expires (or NIL).

**** [slot] =path=

The path this cookie is valid for (or NIL).

**** [slot] =domain=

The domain this cookie is valid for (or NIL).

**** [slot] =secure=

A generalized boolean denoting whether this cookie is a secure cookie.

**** [slot] =http-only=

A generalized boolean denoting whether
this cookie is a `HttpOnly' cookie.

See [[https://owasp.org/www-community/HttpOnly][https://owasp.org/www-community/HttpOnly]]

*** [function] =set-cookie= (name &key (value "") expires max-age path domain secure http-only (reply *reply*))

Creates a cookie object from the parameters provided and adds
it to the outgoing cookies of the REPLY object REPLY.  If a cookie
with the name NAME (case-sensitive) already exists, it is
replaced.

* Epilogue                                                         :noexport:

# Local Variables:
# eval: (auto-fill-mode 1)
# End:

