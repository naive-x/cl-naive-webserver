(in-package :cl-naive-webserver)

(defparameter *sites*
  #+(or sbcl ecl) (make-hash-table :test 'equalp :synchronized nil)
  #+(not (or sbcl ecl)) (make-hash-table :test 'equalp)
  "Where sites are registered.")
;; TODO: question: script-name aka resource-name could be case-sensitive.

(defvar *site*        nil "The current naive-webserver site.")
(defvar *server*      nil "The current naive-webserver server.")
(defvar *session*     nil "The current naive-webserver session.")
(defvar *request*     nil "The current naive-webserver request.")
(defvar *reply*       nil "The current naive-webserver reply.")
(defvar *script-name* nil "The current naive-webserver script name.")
;; You need to bind them around handle-token and handle-site-request where
;; ever you implemented those calls for the specific http backend.

(defvar *debug* nil
  "When true, invoke the debugger upon errors.")

(defvar *report-backtraces* nil
  "When true, error reports will contain a backtrace.
\(This may have security considerations).")


(defgeneric handlers (site))

(defgeneric register-resource (site resource)
  (:documentation "Register resource with site."))

(defgeneric find-resource (site resource-url)
  (:documentation "Return resource func"))

(defgeneric (setf find-resource) (new-resource site resource-url)
  (:documentation "Sets the resource func"))

(defgeneric remove-resource (site resource-url)
  (:documentation "Remove the resource"))

(defgeneric handle-resource-request (resource script-name)
  (:documentation "Handle the request *REQUEST* in the *SESSION*."))


(defclass site ()
  ((url :initarg :url
        :initform "/"
        :accessor url
        :documentation "The external url to the site.")
   (sessions :initarg :sessions
             :initform
             #+(or sbcl ecl) (make-hash-table :test 'equalp :synchronized t)
             #+(not (or sbcl ecl)) (make-hash-table :test 'equalp)
             :accessor sessions
             :documentation "Active sessions for the site. Timed out sessions are removed.")
   (sessions-p :initarg :sessions-p
               :initform nil
               :accessor sessions-p
               :documentation "Indicate if sessions should be enabled for the site.")
   (session-timeout :initarg :session-timeout
                    :initform 3600
                    :accessor session-timeout
                    :documentation "Session timeout in milliseconds.")
   (session-class :initarg :session-class
                  :initform 'session
                  :accessor session-class
                  :documentation "Class used to instanciate session objects.")
   (credentials-class :initarg :credentials-class
                      :initform 'credentials
                      :accessor credentials-class
                      :documentation "Class used to instanciate credentials objects.")
   (handlers :initarg :handlers
             :initform
             #+(or sbcl ecl) (make-hash-table :test 'equalp :synchronized t)
             #+(not (or sbcl ecl)) (make-hash-table :test 'equalp)
             :accessor handlers
             :documentation "Handlers for the site.
Keys are script-names.
Handlers are looked up and called from handle-site-request.")
   (handling-exclusions :initarg handling-exclusions
                        :initform  '(".js"
                                     ".css"
                                     ".jpg"
                                     ".png"
                                     ".gif"
                                     ".ico"
                                     ".svg"
                                     ".woff"
                                     ".woff2"
                                     ".ttf"
                                     "file-upload")
                        :accessor handling-exclusions
                        :documentation "Because lisp webservers are usually run behind a proxy like nginx we exclude the handling of the normal list of suspects that would be served by the proxy and not the lisp server.

If you dont want any exclusions just set this to nil or add your own exclusions.

")))

(defmethod print-object ((site site) stream)
  (print-unreadable-object (site stream :type t :identity t)
    (format stream "~S"
            (list :url (url site)
                  :sessions (sessions site)
                  :sessions-p (sessions-p site)
                  :session-timeout (session-timeout site)
                  :session-class (session-class site)
                  :credentials-class (credentials-class site)
                  :handlers (alexandria:hash-table-alist (handlers site))
                  :handling-exclusions (handling-exclusions site))))
  site)

(defgeneric register-site (site)
  (:documentation "Stores site for retrieval later."))

(defun normalize-site-url (site-url)
  (if (and (plusp (length site-url))
           (char= #\/ (aref site-url 0)))
    (subseq site-url 1)
    site-url))

(defun normalize-resource-name (resource-name)
  (if (and (plusp (length resource-name))
           (char= #\/ (aref resource-name 0)))
    (subseq resource-name 1)
    resource-name))

(defmethod register-site (site)
  (setf (gethash (normalize-site-url (url site)) *sites*) site))

(defgeneric deregister-site (site)
  (:documentation "Removes site from list of registered sites."))

(defmethod deregister-site (site)
  (remhash (normalize-site-url (url site)) *sites*))

(defgeneric get-site (url &optional sites)
  (:documentation "Get a registered site."))

(defmethod get-site ((url string) &optional (sites *sites*))
  (gethash (normalize-site-url url) sites))

(defclass server ()
  ((id :initarg :id
       :initform nil
       :accessor id
       :documentation "Any identifier that is unique.")
   (port :initarg :port
         :initform 5000
         :accessor port
         :documentation "Port used by server.")
   (address :initarg :address
            :initform "0.0.0.0"
            :accessor address
            :documentation "IP address")
   (request-class :initarg :request-class
                  :initform 'request
                  :accessor request-class
                  :documentation "Class used to instanciate request objects.")
   (status :initform :idle :type (member :idle :running) :reader status))
  (:documentation "Used to set some basic info needed by http server."))

(defmethod print-object ((server server) stream)
  (print-unreadable-object (server stream :type t :identity t)
    (format stream "~S" (list :id (id server)
                              :port (port server)
                              :request-class (request-class server))))
  server)

(defparameter *servers*
  #+(or sbcl ecl) (make-hash-table :test 'equalp :synchronized nil)
  #+(not (or sbcl ecl)) (make-hash-table :test 'equalp)
  "Where servers are registered.")

(defun find-server-by-address-port (address port)
  "If address is null, then only port is used.
If port is null, then only address is used.
Both cannot be null."
  (check-type address (or null string integer))
  (check-type port (or null (integer 0 65535)))
  (unless (or address port)
    (error "~S: ADDRESS and PORT cannot be both NULL"
           'find-server-by-address-port))
  (flet ((selectedp (server)
           (and (if address (equalp address (address server)) t)
                (if port    (= port (port server))            t))))
    (maphash (lambda (key server)
               (declare (ignore key))
               (when (selectedp server)
                 (return-from find-server-by-address-port server)))
             *servers*)
    nil))

(defgeneric register-server (server)
  (:documentation "Stores server for retrieval later. You can have more than on server and more than one type of server (ie different back end). A site can only be served by a single server. The server uses the url of a request on the port it serves to select a site based on its site url. Once it selected a site it looks for a specific handler for the url."))

(defmethod register-server ((server server))
  (setf (gethash (id server) *servers*) server))

(defgeneric deregister-server (server)
  (:documentation "Stop server and removes server from list of available servers.
The default implementation does not check to see if any sites are still in use."))

(defmethod deregister-server (server)
  (remhash (id server) *servers*))

(defgeneric get-server (id &optional servers)
  (:documentation "Get a registered server."))

(defmethod get-server (id &optional (servers *servers*))
  (gethash id servers))

(defgeneric start-server (server &key &allow-other-keys)
  (:documentation "Starts a http server to process requests."))

(defmethod start-server (server &key &allow-other-keys)
  (setf (slot-value server 'status) :running)
  server)

(defgeneric stop-server (server &key &allow-other-keys)
  (:documentation "Stops http server processing requests.")
  (:method ((server server) &key &allow-other-keys)
    (setf (slot-value server 'status) :idle)
    nil))

(defclass request ()
  ((back-end-request :initarg :back-end-request
                     :initform nil
                     :accessor back-end-request
                     :documentation "Different http server backends have different request objects. What ever is considered the request object of the back-end is passed along in instances of the naive-webserver request object which is a sanitized version of a typical http request.

If you find your-self going to the back-end-request regularly then you should extend the request class to cover your scenario and specialize make-request for each type of back end. So when you do decide to change back ends that you dont have code every where that needs changing. A little work upfront will save you grief later on! And if you think your tweak would benefit others log and issue to get the default request extended.")
   (headers :initarg :headers
            :initform nil
            :accessor headers
            :documentation "Headers received.")
   (uri :initarg :uri
        :reader uri
        :documentation "The full URI of the request.")
   (script-name :initarg :script-name
                :initform nil
                :accessor script-name
                :documentation "The URI requested by the client without the query string.")
   (server-protocol :initarg :server-protocol
                    :initform nil
                    :accessor server-protocol
                    :documentation "The HTTP protocol as a keyword.")
   (remote-address :initarg :remote-address
                   :initform nil
                   :accessor remote-address
                   :documentation "The IP address of the client that initiated this request.")
   (remote-port :initarg :remote-port
                :initform nil
                :accessor remote-port
                :documentation "The TCP port number of the client socket from which this request originated.")
   (query-string :initarg :query-string
                 :initform nil
                 :accessor query-string
                 :documentation "The query string of this request."))
  (:documentation "A sanitized/naive version of what is considered a request object by the http backends supported."))

(defgeneric make-request (server raw-request)
  (:documentation "Take the backend request and create a sanitized request. Needs to be implemented for each backend."))

(defclass reply ()
  ((return-code :initarg :return-code :accessor return-code)
   (headers :initarg :headers :accessor headers
            :documentation "The headers sent back with the reply.
This should be an a-list of keywords mapping to values."))
  (:documentation "This class is a wrapper over the native reply class."))

(defgeneric make-reply (server raw-reply)
  (:documentation "Take the backend reply and create a sanitized reply or reply wrapper. Needs to be implemented for each backend."))

(defclass credentials ()
  ()
  (:documentation "This is a class used to store user credentials in. Used during the authorization process.

The user needs to specialise this class and set the credetials-class of the site instance to your specialization.

populate-credentials needs to be implemented for the specialization to populate an instance of this class from a request.

make-credentials should not need specialization but can be specialized."))

(defclass credentials-basic (credentials)
  ((user-name :initarg :user-name
              :initform nil
              :accessor user-name
              :documentation "User identifier.")
   (password :initarg :password
             :initform nil
             :accessor password
             :documentation "Password"))
  (:documentation "A basic user-name password credentials class. It is still up to the user of the package to implement authenticate specialised on credentials-basic since this package has no concept of what to authenticate against."))

(defgeneric populate-credentials (credentials request)
  (:documentation "Populates credentials from request. This needs to be specialized if you are not going to use credentials-basic. "))

(defmethod populate-credentials (credentials request)
  (declare (ignore request))
  credentials)

(defmethod populate-credentials ((credentials credentials-basic) request)
  "Uses user-name and password parameters to populate a credentials-basic instance.

populate and make-credentials are split this way because the implementation creates the credentials instance base on the site's credentials class."
  (declare (ignore request))
  (setf (user-name credentials) (parameter "user-name"))
  (setf (password credentials) (parameter "password"))
  credentials)

(defgeneric make-credentials (site request)
  (:documentation "Creates credentials based on credentials-class of the site and calls populate-credentials to populate the values from the request."))

(defmethod make-credentials (site request)
  (when (credentials-class site)
    (populate-credentials (make-instance (credentials-class site)) request)))

(defclass authorization ()
  ((credentials :initarg :credentials
                :initform nil
                :accessor credentials
                :documentation "Holds an instance of the credentials used during authorization of then caller.")
   (remote-address :initarg :remote-address
                   :initform nil
                   :accessor remote-address
                   :documentation "Then callers real IP address")
   (time-stamp :initarg :time-stamp
               :initform nil
               :accessor time-stamp
               :documentation "Time stamp."))
  (:documentation "An authorization object is created during authentication. The default class holds the credentials instance and some other basic information about the authorization. Each session has an authorization instance.

Specialize this if you need some thing more complex. You will also have to specialize start-session and authenticate which should return an authorization instance."))

(defgeneric authenticate (credentials &key &allow-other-keys)
  (:documentation "Must check the credentials of caller and return an authorization instance if authentication.

The default implementation ignores the credentials and always returns a authorization instance. Credentials and authentication must be specialised and implemented by the user."))

(defmethod authenticate (credentials &key &allow-other-keys)
  (make-instance 'authorization
                 :credentials credentials))

(defmethod authenticate ((credentials null) &key &allow-other-keys)
  t)

(defclass session ()
  ((site :initarg :site
         :initform nil
         :accessor site
         :documentation "Reference to site for convenience.")
   (authorization :initarg :authorization
                  :initform nil
                  :accessor authorization
                  :documentation "An object that represents the authorization of the api caller.")
   (last-click :initarg :last-click
               :initform nil
               :accessor last-click
               :documentation "Last time the user was active in the session.")
   (cache :initarg :cache
          :initform
          #+(or sbcl ecl) (make-hash-table :test 'equalp :synchronized nil)
          #+(not (or sbcl ecl)) (make-hash-table :test 'equalp)
          :accessor cache
          :documentation "A place to cache session values. Doing caching on session level, so that memory can be release when the session dies."))
  (:documentation "To access web resources the caller must first establish a session, with the correct authentication credentials and then use the token issued for the session to service further requests.

naive-webserver by passes any session handling done by http backends prefering its own implementation of sessions. This is because we implement token based security by default. It also makes it easier to allow extentions of the session functionality."))

(defun session-value (key &optional (session *session*))
  (when *session*
    (gethash key (cache session))))

(defsetf session-value (key &optional (session *session*))
    (new-value)
  `(let ((session (or ,session *session*)))
     (if session
         (setf (gethash ,key (cache session)) ,new-value)
         (error "No Session!"))))

(defgeneric issue-token (site &key authorization)
  (:documentation "If authorization pass checks then issue a token for use with further requests for the session.

The default implementation ignores the authorization and always returns a token so that sessions can be used without explicit authentication. It returns a blake2 mac using ironclad, it is complete
overkill because tokens are dumb values and requires no fancy reversible calculations."))

(defmethod issue-token (site &key authorization)
  (declare (ignorable authorization site))
  (let ((key (ironclad:random-data 8)))
    (ironclad:byte-array-to-hex-string
     (ironclad:produce-mac (ironclad:make-blake2-mac key)))))

(defgeneric start-session (site credentials &key token &allow-other-keys)
  (:documentation "If a session for the token already exists it returns existing the existing session.

If no session exists for the token it creates a new session based on the session-class of the site. It runs authenticate against the credentials and uses issues-token to issue a token if the authentication passed.

Start-session should only ever be called from handle-token, which is called on a explicit token request."))

(defmethod start-session (site credentials &key token &allow-other-keys)
  (let* ((authorization (authenticate credentials))
         (token (or token (if authorization
                              (issue-token authorization))))
         (session (gethash token (sessions site))))

    (unless authorization
      (return-from start-session (values nil nil "Authentication failed.")))

    (when (and token (not session))
      (setf session (make-instance (session-class site)
                                   :site site
                                   :authorization authorization
                                   :last-click (get-universal-time)))
      (setf (gethash token (sessions site)) session))

    (values session token nil)))

(defgeneric get-session (site token)
  (:documentation "Retrieves the session using the token supplied. If the session has timed out or does not exit no session is returned."))

(defmethod get-session (site token)
  (let ((session (gethash token (sessions site))))
    (cond
      ((null session)
       (values nil nil))
      ((> (last-click session) (- (get-universal-time) (* (session-timeout site) 60)))
       (values session nil))
      (t
       (remhash token (sessions site))
       (set-cookie "Auth-Token"
                   :value "Session Expired"
                   :path "/"
                   :expires (+ (last-click session) (* (session-timeout site) 60)))
       (values nil "Session Expired")))))

(defgeneric handle-token (site request credentials &key &allow-other-keys)
  (:documentation "Handles a request for a token and starts a session. It is called on url=/token by default.

It returns the token to caller and sets a cookie \"Auth-Token\" on the outgoing reply."))

(defmethod handle-token (site request credentials &key &allow-other-keys)
  (declare (ignorable request))
  (multiple-value-bind (session token error)
      (start-session site credentials)
    (when (and session (not error)) 
      (set-cookie "Auth-Token"
                  :value token
                  :path "/"
                  :http-only t))
    (values token session error)))

(defun webserver-special-bindings (&optional (default-special-bindings 
                                              bt:*default-special-bindings*))
  "A list of bindings for the threads used to handle the naive-webserver requests."
  (acons '*site* *site*
         (acons '*session* *session*
                (acons '*request* *request*
                       (acons '*reply* *reply*
                              (acons '*script-name*  *script-name*
                                     default-special-bindings))))))


(defun report-error (condition)
  "Builds and log an error message to report the CONDITION, with a backtrace when *REPORT-BACKTRACE* is true.

RETURN: the error message.
"
  (log-message :error *server*
               "naive-webserver got an error while processing the request ~S:~%~A~@[~%~A~]"
               (and *request* (uri *request*))
               condition
               (and *report-backtraces*
                    (with-output-to-string (out)
                      (handler-case
                          (uiop:print-backtrace :stream out
                                                :condition condition)
                        (error (err)
                          (format out "~&~A" err)))))))

(defun call-with-error-reporting (thunk)
  (loop
    :named retry
    :do (restart-case
            (handler-bind
                ((error
                   (lambda (condition)
                     (let ((message (report-error condition)))
                       (when *debug*
                         (invoke-debugger condition))
                       (setf (return-code *reply*) 500)
                       (push (cons :content-type "text/plain") (headers *reply*))
                       (return-from retry message)))))
              (return-from retry (funcall thunk)))
          (retry ()
            :report "retry processing http request"))))

(defmacro with-error-reporting (&body body)
  "
This macro wraps the BODY in a HANDLER-BIND to catch errors
and report them to the hunchentoot server, with a HTTP status code,
and a HTML page describing the error.
Set *DEBUG* to true to invoke the debugger on error.
"
  `(call-with-error-reporting (lambda () ,@body)))

(defmethod url ((n null))
  (let ((*report-backtraces* t))
   (report-error (make-condition 'simple-error :format-control "No method for URL NIL"))))

(defgeneric parameter (name &key type)
  (:documentation "Returns a get or post parameter by name unless type of :get or :post is supplied to get as specific parameter type."))

(defgeneric handle-site-request (site request &key &allow-other-keys)
  (:documentation "The handle-site-request method looks up a function in the handlers slot for the site and then calls the handler.

By the time the handle-site-request method is called authorization has already been dealt with.

*session* and *request* instances are bound for duration of the call to handle-site-request. This is done in an :around.

The default implementation uses the script-name to lookup the handler to call."))



(defmethod handle-site-request :around (site request &key session &allow-other-keys)
  (let ((where '(handle-site-request :around t t)))
    (log-message :info site "~S Handling request for site ~S and request ~S" where (url site) (uri request))
    (assert (and (boundp '*server*) *server*))
    (let* ((*session*   session)
           (*request*   (make-request *server* request))
           (*site*      site))
      (log-message :debug site "~S Server:   ~A" where *server*)
      ;; cl-naive-server.hunchentoot:*acceptor* should be = (acceptor *server*)
      (log-message :debug site "~S Request:  ~A" where *request*)
      (log-message :debug site "~S Session:  ~A" where *session*)
      (log-message :debug site "~S Site:     ~A" where *site*)
      (when session                     ; Keep session alive
        (setf (last-click session) (get-universal-time)))
      (when (next-method-p)
        (call-next-method)))))

(defgeneric site-url (site url))

(defmethod site-url ((site site) url)
  (format nil "~A~A" (url site) url))

(defun parse-site-url (url)
  (let ((split (split-sequence:split-sequence #\/ url)))
    (if (= (length split) 2)
        (list :site "/" :url (format nil "/~A" (second split)))
        (list :site (format nil "/~A" (second split))
              :url (format nil "/~{~a~^/~}" (cddr split))))))

(defgeneric bad-request (request &key &allow-other-keys)
  (:documentation "Return http codes and messages to indicate that a request url could not be handled."))

(defgeneric unauthorized-request (request &key &allow-other-keys)
  (:documentation "Return http codes and messages to indicate that a request is not authorized."))


(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *log-file-pathname* #P"/tmp/naive-webserver.log"))

(defmethod return-code ((list list))
  (first list))

