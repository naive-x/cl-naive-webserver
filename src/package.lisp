(in-package :common-lisp-user)

(defpackage :cl-naive-webserver
  (:use :cl :cl-getx :cl-naive-log)
  (:export

   :site
   :url
   :sessions
   :sessions-p
   :sessions-timeout
   :sessions-class
   :get-session
   :credentials-class
   :handlers
   :handling-exclusions

   :register-site
   :deregister-site
   :get-site

   :resource :url :register-resource :find-resource 
   :remove-resource :handle-resource-request
   :normalize-site-url :normalize-resource-name
   
   :server
   :port
   :address
   :request-class
   :status
   :register-server
   :deregister-server
   :find-server-by-address-port
   :get-server
   :start-server
   :stop-server

   :request
   :back-end-request
   :headers
   :script-name
   :server-protocol
   :remote-address
   :remote-port
   :query-string

   :credentials
   :credentials-basic
   :user-name
   :password

   :populate-credentials
   :make-credentials

   :authorization
   :remote-address
   :time-stamp

   :session
   :last-click
   :cache

   :authenticate
   :issue-token

   :start-session
   :get-sesssion
   :session-value

   :bad-request
   :handle-token
   :parameter
   :handle-site-request
   :make-request
   :make-reply
   :reply
   :return-code
   
   :*debug*
   :*report-backtraces*
   :*site*
   :*script-name*
   :*server*
   :*session*
   :*request*
   :*reply*
   :webserver-special-bindings
   :with-error-reporting
   :report-error
   :*handling-exclusions*
   :*token-url*

   :cookie
   :cookie-name
   :cookie-value
   :cookie-expires
   :cookie-max-age
   :cookie-path
   :cookie-domain
   :cookie-secure
   :cookie-http-only
   :set-cookie*
   :set-cookie
   :stringify-cookie-value)

  (:export
   :site-url
   :parse-site-url))

