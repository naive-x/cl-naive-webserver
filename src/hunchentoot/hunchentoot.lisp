(in-package :cl-naive-webserver.hunchentoot)


(defclass hunchentoot-server (server)
  ((acceptor :initform nil
             :accessor acceptor
             :documentation "The hunchentoot acceptor that will be used by the server.")))


(defmethod print-object ((server hunchentoot-server) stream)
  (print-unreadable-object (server stream :type t :identity t)
    (let ((listening (and (acceptor server)
                          (hunchentoot::acceptor-listen-socket (acceptor server)))))
      (format stream "~S" 
              (list :id (slot-value server 'cl-naive-webserver::id)
                    :port (slot-value server 'cl-naive-webserver::port)
                    :request-class (slot-value server 'cl-naive-webserver::request-class)
		    :status (if listening :listening :idle)))))
  server)

;;;
;;; hunchentoot-acceptor for cl-naive-webserver
;;;

;; The principle of operation is to use a chain of mixins primary
;; methods (using call-next-method) on ACCEPTOR-DISPATCH-REQUEST, to
;; dispatch and process the request.  (Previously, we overrode the
;; whole HANDLE-REQUEST method).  Using those mixins let us insert new
;; mixins, such as the WEBSOCKETS-ACCEPTOR-MIXIN.


(defclass exclusion-acceptor-mixin ()
  ()
  (:documentation "Use this mixin to process exclusions, ie. explicit path to resource stored in files."))

(defclass token-acceptor-mixin ()
  ((resource-name :initarg :resource-name :reader resource-name))
  (:default-initargs :resource-name "/token")
  (:documentation "Use this mixin to process the `token' resource."))

(defclass session-acceptor-mixin ()
  ()
  (:documentation "Use this mixin to retrieve the current session (initialized by access to the token resource)."))

(defclass site-acceptor-mixin ()
  ()
  (:documentation "Use this mixin to handle the request using the *site* instance."))

(defclass hunchentoot-acceptor (exclusion-acceptor-mixin
                                token-acceptor-mixin
                                session-acceptor-mixin
                                site-acceptor-mixin
                                hunchentoot:acceptor)
  ((server :initarg :server
           :initform nil
           :accessor server
           :documentation "The naive-webserver bound to this acceptor.")))

(defmethod print-object ((acceptor hunchentoot-acceptor) stream)
  (print-unreadable-object (acceptor stream :identity t :type t)
    (format stream ":SERVER ~S" (server acceptor)))
  acceptor)


(defmethod hunchentoot:acceptor-dispatch-request :around ((acceptor hunchentoot-acceptor) request)
  (with-error-reporting
    (let ((where '(hunchentoot:acceptor-dispatch-request :around hunchentoot-acceptor t)))
      (log-message :debug acceptor "~S hunchentoot:*acceptor* ~A" where hunchentoot:*acceptor*)
      (let* ((*acceptor*    acceptor)
	     (*server*      (server *acceptor*))
             (urls          (parse-site-url (hunchentoot:script-name request)))
             (*reply*       (make-reply *server* hunchentoot:*reply*))
             (*site*        (get-site (getf urls :site)))
             (*script-name* (getf urls :url)))
        (log-message :debug acceptor "~S *acceptor* ~A" where *acceptor*)
        (log-message :debug acceptor "~S *server* ~A" where *server*)
        (log-message :debug acceptor "~S *reply* ~A" where *reply*)
        (log-message :debug acceptor "~S *site* ~A" where  *site*)
        (log-message :debug acceptor "~S *script-name* ~S" where  *script-name*)
        (unwind-protect
             (when (next-method-p)
               (call-next-method))
          (log-message :debug acceptor "~S done." where))))))


(defmethod hunchentoot:acceptor-dispatch-request ((acceptor exclusion-acceptor-mixin) request)
  (let ((where '(hunchentoot:acceptor-dispatch-request exclusion-acceptor-mixin t)))
    (log-message :debug acceptor "~S acceptor = ~A" where acceptor)
    (log-message :debug acceptor "~S request = ~A" where request)
    (log-message :debug acceptor "~S *site* = ~A" where *site*))
  (flet ((next () (when (next-method-p) (call-next-method))))
    (if (null *site*)
        (next)
        (let ((default-exclusions (handling-exclusions *site*)))
          (if (find *script-name* default-exclusions
                    :test (lambda (name exclusion)
                            (search exclusion name :test 'equalp)))
              (let ((file (find-resource (get-site
                                          (getf (parse-site-url
                                                 (hunchentoot:script-name request))
                                                :site))
                                         *script-name*)))
                (if file
                    (hunchentoot:handle-static-file file)
                    (next)))
              (next))))))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor token-acceptor-mixin) request)
  (let ((where '(hunchentoot:acceptor-dispatch-request token-acceptor-mixin t)))
    (log-message :debug acceptor "~S acceptor = ~A" where acceptor)
    (log-message :debug acceptor "~S request = ~A" where request)
    (log-message :debug acceptor "~S *site* = ~A" where *site*))
  (flet ((next () (when (next-method-p) (call-next-method))))
    (if (null *site*)
        (next)
        (if (equalp *script-name* (resource-name acceptor))
            (multiple-value-bind (token session errorp)
                (handle-token *site* request (make-credentials *site* request))
              (declare (ignore session))
              (if errorp
                  (unauthorized-request request)
                  (values token nil nil)))
            (next)))))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor session-acceptor-mixin) request)
  (let ((where '(hunchentoot:acceptor-dispatch-request session-acceptor-mixin t)))
    (log-message :debug acceptor "~S acceptor = ~A" where acceptor)
    (log-message :debug acceptor "~S request = ~A" where request)
    (log-message :debug acceptor "~S *site* = ~A" where *site*))
  (flet ((next () (when (next-method-p) (call-next-method))))
    (if (null *site*)
        (next)
        (let ((token (or (parameter "auth-token")
                         (hunchentoot:cookie-in "Auth-Token" request))))
          (multiple-value-bind (*session* error) (get-session *site* token)
            ;; (format t "Session Check: not sessions-p ~S :session ~S :error ~A~%"
            ;;         (not (sessions-p *site*)) *session* error)
            (cond
              (error
               (bad-request request))
              ((or (not (sessions-p *site*)) *session*)
               (next))
              (t
               (unauthorized-request request))))))))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor site-acceptor-mixin) request)
  (let ((where '(hunchentoot:acceptor-dispatch-request site-acceptor-mixin t)))
    (log-message :debug acceptor "~S acceptor = ~A" where acceptor)
    (log-message :debug acceptor "~S request = ~A" where request)
    (log-message :debug acceptor "~S *site* = ~A" where *site*))
  ;; Note: this method doesn't call-next-method!  It overrides the method of hunchentoot:acceptor.
  (if *site*
      (handle-site-request *site* request :session *session*)
      (let ((message (format nil "No site found for script-name ~S while processing the request ~S"
                             *script-name*
                             (hunchentoot:script-name request))))
        (log-message :error acceptor "~A" message)
        (setf (hunchentoot:return-code*)  hunchentoot:+http-not-found+
              (hunchentoot:content-type*) "text/plain")
        message)))

(define-condition no-such-resource (error)
  ((site :initarg :site :reader no-such-resource-site)
   (path :initarg :path :reader no-such-resource-path))
  (:report (lambda (condition stream)
             (let ((site  (no-such-resource-site condition)))
               (format stream "There's no resource at path ~S in site ~S"
                       (no-such-resource-path condition)
                       (and site (url site)))))))

(defmethod handle-site-request ((site site) (request hunchentoot:request) &key &allow-other-keys)
  (let ((where '(handle-site-request site hunchentoot:request)))
    (log-message :info site "~S Handling request for site ~S and request ~S" where (url site) (cl-naive-webserver::uri request)))
  (let* ((urls        (parse-site-url (hunchentoot:script-name request)))
         (script-name (getf urls :url))
         (resource    (find-resource site script-name)))
    (cond
      ((equalp script-name "/")
       ;; TODO: Why? (the previous version just did nothing, why?)
       (error 'no-such-resource :path script-name :site site))
      (resource
       (handle-resource-request resource (hunchentoot:script-name request)))
      (t
       (error 'no-such-resource :path script-name :site site)))))

(defmethod set-cookie* ((reply hunchentoot:reply) cookie)
  "Adds the COOKIE object COOKIE to the outgoing cookies of the
REPLY object REPLY. If a cookie with the same name
\(case-sensitive) already exists, it is replaced."
  (let* ((name (cl-naive-webserver.hunchentoot::cookie-name cookie))
         (place (assoc name (hunchentoot:cookies-out reply) :test #'string=)))
    (if place
        (setf (cdr place) cookie)
        (push (cons name cookie) (hunchentoot:cookies-out reply)))
    cookie))

(defmethod hunchentoot::stringify-cookie ((cookie cookie))
  "Converts the COOKIE object COOKIE to a string suitable for a
'Set-Cookie' header to be sent to the client."
  (format nil "~A=~A" (cookie-name cookie) (stringify-cookie-value cookie)))

(defmethod start-server ((server hunchentoot-server) &key &allow-other-keys)
  (unless (acceptor server)
    (setf (acceptor server)
          (make-instance 'hunchentoot-acceptor
                         :port (port server)
                         :address (address server)
                         :server server)))
  (hunchentoot:start (acceptor server))
  (call-next-method)
  (acceptor server))

(defmethod stop-server ((server hunchentoot-server) &key &allow-other-keys)
  (when (and (acceptor server)
             (hunchentoot::acceptor-listen-socket (acceptor server)))
    (hunchentoot:stop (acceptor server))
    (call-next-method)
    (setf (acceptor server) nil)))

(defmethod bad-request ((request request) &key &allow-other-keys)
  (setf (hunchentoot:return-code hunchentoot:*reply*)
        hunchentoot:+http-bad-request+)
  (hunchentoot:abort-request-handler))

(defmethod unauthorized-request ((request request) &key &allow-other-keys)
  (unauthorized-request (back-end-request request)))

(defmethod unauthorized-request ((request hunchentoot:request) &key &allow-other-keys)
  (setf (hunchentoot:return-code hunchentoot:*reply*) hunchentoot:+http-authorization-required+)
  (hunchentoot:abort-request-handler))

(defmethod parameter (name &key type)
  (cond ((equalp type :get)
         (hunchentoot:get-parameter name))
        ((equalp type :post)
         (hunchentoot:post-parameter name))
        (t
         (or (hunchentoot:get-parameter name)
             (hunchentoot:post-parameter name)))))

(defmethod make-request ((server hunchentoot-server) raw-request)
  (make-instance (request-class server)
                 :back-end-request raw-request
                 :uri (hunchentoot:request-uri raw-request)
                 :headers (hunchentoot:headers-in raw-request)
                 :script-name (hunchentoot:script-name raw-request)
                 :server-protocol (hunchentoot:server-protocol raw-request)
                 :remote-address (hunchentoot:remote-addr raw-request)
                 :remote-port (hunchentoot:remote-port raw-request)
                 :query-string (hunchentoot:query-string raw-request)))

(defmethod cl-naive-webserver::uri ((request hunchentoot:request))
  (hunchentoot:request-uri request))

(defclass hunchentoot-reply-wrapper (reply)
  ((back-end-reply :initarg :back-end-reply :reader back-end-reply)))

(defmethod make-reply ((server hunchentoot-server) raw-reply)
  (make-instance 'hunchentoot-reply-wrapper :back-end-reply raw-reply))

(defmethod return-code ((reply hunchentoot-reply-wrapper))
  (hunchentoot:return-code (back-end-reply reply)))

(defmethod (setf return-code) (new-return-code (reply hunchentoot-reply-wrapper))
  (setf (hunchentoot:return-code (back-end-reply reply)) new-return-code))

(defmethod headers ((reply hunchentoot-reply-wrapper))
  (acons :content-type (hunchentoot:content-type (back-end-reply reply))
         (acons :content-length (or (hunchentoot:content-length (back-end-reply reply)) 0)
                (hunchentoot:headers-out (back-end-reply reply)))))

(defmethod (setf headers) (new-headers (reply hunchentoot-reply-wrapper))
  ;; new-headers is an a-list, ealiers pairs shadow later pairs.
  ;; So we process it in reverse order:
  (dolist (header (reverse new-headers) new-headers)
    (setf (hunchentoot:header-out (car header) (back-end-reply reply)) (cdr header))))

(defmethod return-code ((reply hunchentoot:reply))
  (hunchentoot:return-code reply))

(defmethod (setf return-code) (new-return-code (reply hunchentoot:reply))
  (setf (hunchentoot:return-code reply) new-return-code))

(defmethod headers ((reply hunchentoot:reply))
  (acons :content-type (hunchentoot:content-type reply)
         (acons :content-length (or (hunchentoot:content-length reply) 0)
                (hunchentoot:headers-out reply))))

(defmethod (setf headers) (new-headers (reply hunchentoot:reply))
  ;; new-headers is an a-list, ealiers pairs shadow later pairs.
  ;; So we process it in reverse order:
  (dolist (header (reverse new-headers) new-headers)
    (setf (hunchentoot:header-out (car header) reply) (cdr header))))

(defmethod set-cookie* ((reply hunchentoot-reply-wrapper) cookie)
  (set-cookie* (back-end-reply reply) cookie))

(defmethod handle-resource-request ((resource pathname) script-name)
  (let ((where '(handle-resource-request pathname t)))
    (log-message :info resource "~S Handling resource request with file ~A and script-name ~S" where resource script-name)
    (hunchentoot:handle-static-file resource)))

;;;; THE END ;;;;
