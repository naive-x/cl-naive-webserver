(in-package :common-lisp-user)

(defpackage :cl-naive-webserver.hunchentoot
  (:use :cl :cl-getx :cl-naive-log :cl-naive-webserver)
  (:import-from :hunchentoot :*acceptor*)
  (:export

   :hunchentoot-server

   :start-server
   :stop-server
   :bad-request
   :unauthorized-request
   :parameter
   :make-request
   
   :*acceptor*
   :acceptor
   :exclusion-acceptor-mixin
   :token-acceptor-mixin
   :session-acceptor-mixin
   :site-acceptor-mixin
   :hunchentoot-acceptor)
  (:export
   :*report-backtraces*))

