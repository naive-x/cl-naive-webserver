(in-package :cl-naive-webserver)

(defclass resource ()
  ((url :initarg :url
        :initform nil
        :accessor url
        :documentation "Resource url.")
   (func :initarg :func
         :initform nil
         :accessor func
         :documentation "Resource function.")
   (websocket-p :initarg :websocket-p
                :initform nil
                :accessor websocket-p
                :documentation "Resource uses websockets."))
  (:documentation "Class that represents site resource aka page."))

(defmethod print-object ((resource resource) stream)
  (print-unreadable-object (resource stream :type t :identity t)
    (format stream "~S" (list :url (url resource)
                              :func (func resource)
                              :websocket-p (websocket-p resource))))
  resource)

(defmethod register-resource ((site site) (resource resource))
  (setf (find-resource site (url resource)) resource))

(defmethod register-resource ((handlers hash-table) (resource resource))
  (setf (find-resource handlers (url resource)) resource))

(defmethod (setf find-resource) (new-resource (site site) resource-url)
  (setf (find-resource (handlers site) resource-url) new-resource))

(defmethod (setf find-resource) (new-resource (handlers hash-table) resource-url)
  (setf (gethash (normalize-resource-name resource-url)
                 handlers)
        new-resource))

(defmethod find-resource ((nothing null) resource-url)
  (declare (ignorable nothing resource-url))
  nil)

(defmethod find-resource ((site site) resource-url)
  (find-resource (handlers site) resource-url))

(defmethod find-resource ((handlers hash-table) resource-url)
  (or (gethash resource-url handlers)
      (let ((normalized (normalize-resource-name resource-url)))
        (or (gethash normalized handlers)
            (gethash (concatenate 'string "/" normalized) handlers)))))

(defmethod remove-resource ((nothing null) resource-url)
  (declare (ignorable nothing resource-url))
  nil)

(defmethod remove-resource ((site site) resource-url)
  (remove-resource (handlers site) resource-url))

(defmethod remove-resource ((handlers hash-table) resource-url)
  (remhash resource-url handlers))

(defun post-process-resource-request-result (where resource result)
  (let ((result (if (typep result '(or string (vector (unsigned-byte 8))))
                    result
                    (with-standard-io-syntax
                      (prin1-to-string result)))))
    ;; Important to use ~A for the result here, because when it's not printable readably, we don't want to get an error.
    (log-message :debug resource "~S Result from resource handling function ~A is ~A" where resource result)
    result))

(defmethod handle-resource-request ((resource resource) script-name)
  (let ((where '(handle-resource-request resource t)))
    (log-message :info resource "~S Handling resource request for resource ~S and script-name ~S" where (url resource) script-name)
    (post-process-resource-request-result where resource (funcall (func resource) script-name))))

(defmethod handle-resource-request ((resource function) script-name)
  (let ((where '(handle-resource-request function t)))
    (log-message :info resource "~S Handling resource request with function ~A and script-name ~S" where resource script-name)
    (post-process-resource-request-result where resource (funcall resource script-name))))

