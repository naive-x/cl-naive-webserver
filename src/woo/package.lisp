(in-package :common-lisp-user)

(defpackage :cl-naive-webserver.woo
  (:use :cl :cl-getx)
  (:export

   :woo-server
   :make-request
   :bad-request
   :handle-request
   :start-server))

