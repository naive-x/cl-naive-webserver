(defsystem "cl-naive-webserver.hunchentoot.tests.server"
  :description "A test server for cl-naive-webserver.hunchentoot.tests."
  :version "2023.4.18"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:usocket :cl-naive-webserver.hunchentoot :cl-naive-tests)
  :components ((:file "tests/usocket-patches")
               (:file "tests/hunchentoot/package")
               (:file "tests/hunchentoot/server" :depends-on ("tests/hunchentoot/package"))))

