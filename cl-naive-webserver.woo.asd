(defsystem "cl-naive-webserver.woo"
  :description ""
  :version "2022.9.19"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-webserver
               :cl-who
               :bordeaux-threads
               :woo)
  :components ((:file "src/woo/package" :depends-on ())
               (:file "src/woo/woo"     :depends-on ("src/woo/package"))))
