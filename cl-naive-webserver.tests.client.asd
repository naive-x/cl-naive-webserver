(defsystem "cl-naive-webserver.tests.client"
  :description "Test client for cl-naive-webserver."
  :version "2023.12.6"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:drakma)
  :components ((:file "tests/client")))
