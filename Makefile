all: test documentation
.PHONY:: test tests documentation clean

# default values:
ARTDIR = tests/artifacts/
DEPENDENCYDIR = $(abspath $(CURDIR)/..)/
THISDIR = $(abspath $(CURDIR))/
export DEPENDENCYDIR

# DEBUG_TEST = NIL
DEBUG_TEST = T

# To enable debugging in sbcl (not needed for ccl)):
SBCL_DEBUG = --eval "(sb-ext:enable-debugger)" # --eval '(sb-thread:release-foreground)'

PORT = 33001
# MINIMAL_LOG_LEVEL = debug
MINIMAL_LOG_LEVEL = info

LOG_FILE = $(ARTDIR)server.log

help:
	@printf 'make %-16s # %s\n' documentation 'Generate documentation.'
	@printf 'make %-16s # %s\n' test          'Run the server and tests.'
	@printf 'make %-16s # %s\n' test-alone    'Run just the tests, assuming make server is running.'
	@printf 'make %-16s # %s\n' test-ccl      'Run the server and tests on ccl.'
	@printf 'make %-16s # %s\n' test-sbcl     'Run the server and tests on sbcl.'
	@printf 'make %-16s # %s\n' server        'Launches a test server.'
	@printf 'make %-16s # %s\n' server-ccl    'Launches a test server on ccl.'
	@printf 'make %-16s # %s\n' server-sbcl   'Launches a test server on sbcl.'
	@printf 'make %-16s # %s\n' clean         'Clean temporary files.'

documentation:
	make -C docs pdfs

test tests:test-sbcl

$(LOG_FILE):
	mkdir -p $$(dirname "$(LOG_FILE)")
	touch    "$(LOG_FILE)"

test-sbcl: $(LOG_FILE)
	sbcl \
		--dynamic-space-size 4096 \
		--noinform --no-userinit --non-interactive \
		$(SBCL_DEBUG) \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-webserver.hunchentoot.tests)' \
		--eval '(in-package :CL-NAIVE-WEBSERVER.HUNCHENTOOT.TESTS)' \
		--eval '(setf *port* $(PORT))' \
		--eval "(setf cl-naive-tests:*debug*   $(DEBUG_TEST))" \
		--eval "(setf cl-naive-tests:*verbose* $(DEBUG_TEST))" \
		--eval '(setf cl-naive-webserver:*report-backtraces* t)' \
		--eval "(setf cl-naive-log:*minimal-log-level* :$(MINIMAL_LOG_LEVEL))"  \
		--eval '(setf cl-naive-log:*log-file-pathname* (truename #P"$(LOG_FILE)"))' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text)' \
		--eval '(cl-naive-tests:save-results cl-naive-tests:*suites-results* :file "$(ARTDIR)junit-results.xml" :format :junit)' \
		--eval '(sb-ext:exit :code (if (cl-naive-tests:report) 0 200))'

test-alone: $(LOG_FILE)
	sbcl \
		--dynamic-space-size 4096 \
		--noinform --no-userinit --non-interactive \
		$(SBCL_DEBUG) \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-webserver.hunchentoot.tests)' \
		--eval '(in-package :CL-NAIVE-WEBSERVER.HUNCHENTOOT.TESTS)' \
		--eval '(setf cl-naive-webserver.hunchentoot.tests:*start-own-server* nil)' \
		--eval '(setf *port* $(PORT))' \
		--eval "(setf cl-naive-tests:*debug*   $(DEBUG_TEST))" \
		--eval "(setf cl-naive-tests:*verbose* $(DEBUG_TEST))" \
		--eval '(setf cl-naive-webserver:*report-backtraces* t)' \
		--eval "(setf cl-naive-log:*minimal-log-level* :$(MINIMAL_LOG_LEVEL))"  \
		--eval '(setf cl-naive-log:*log-file-pathname* (truename #P"$(LOG_FILE)"))' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text)' \
		--eval '(cl-naive-tests:save-results cl-naive-tests:*suites-results* :file "$(ARTDIR)junit-results.xml" :format :junit)' \
		--eval '(sb-ext:exit :code (if (cl-naive-tests:report) 0 200))'

test-ccl: $(LOG_FILE)
	ccl \
		--quiet --no-init \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-webserver.hunchentoot.tests)' \
		--eval '(in-package :CL-NAIVE-WEBSERVER.HUNCHENTOOT.TESTS)' \
		--eval '(setf *port* $(PORT))' \
		--eval "(setf cl-naive-tests:*debug*   $(DEBUG_TEST))" \
		--eval "(setf cl-naive-tests:*verbose* $(DEBUG_TEST))" \
		--eval '(setf cl-naive-webserver:*report-backtraces* t)' \
		--eval "(setf cl-naive-log:*minimal-log-level* :$(MINIMAL_LOG_LEVEL))"  \
		--eval '(setf cl-naive-log:*log-file-pathname* (truename #P"$(LOG_FILE)"))' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:run)' \
		--eval '(cl-naive-tests:write-results cl-naive-tests:*suites-results* :format :text)' \
		--eval '(cl-naive-tests:save-results cl-naive-tests:*suites-results* :file "$(ARTDIR)junit-results.xml" :format :junit)' \
		--eval '(ccl:exit (if (cl-naive-tests:report) 0 200))'

server:server-sbcl

server-sbcl: $(LOG_FILE)
	sbcl \
		--dynamic-space-size 4096 \
		--noinform --no-userinit \
		--eval '(push :use-ansi-colors *features*)' \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-webserver.hunchentoot.tests.server)' \
		--eval '(in-package :CL-NAIVE-WEBSERVER.HUNCHENTOOT.TESTS)' \
		--eval '(setf *port* $(PORT))' \
		--eval "(setf cl-naive-log:*minimal-log-level* :$(MINIMAL_LOG_LEVEL))"  \
		--eval '(setf cl-naive-log:*log-file-pathname* (truename #P"$(LOG_FILE)"))' \
		--eval '(setf cl-naive-webserver:*report-backtraces* t)' \
		--eval '(let ((initfile #P"~/.cl-naive-webserver-test-server.lisp")) (when (probe-file initfile) (load initfile)))' \
		--eval '(cl-naive-webserver.hunchentoot.tests::run-server)' 

server-ccl: $(LOG_FILE)
	ccl \
		--quiet --no-init \
		--eval '(push :use-ansi-colors *features*)' \
		--eval '(load #P"~/quicklisp/setup.lisp")' \
		--eval '(push "$(DEPENDENCYDIR)" ql:*local-project-directories*)' \
		--eval '(push #P"$(THISDIR)" asdf:*central-registry*)' \
		--eval '(ql:quickload :cl-naive-webserver.hunchentoot.tests.server)' \
		--eval '(in-package :CL-NAIVE-WEBSERVER.HUNCHENTOOT.TESTS)' \
		--eval '(setf *port* $(PORT))' \
		--eval '(setf cl-naive-webserver:*report-backtraces* t)' \
		--eval "(setf cl-naive-log:*minimal-log-level* :$(MINIMAL_LOG_LEVEL))"  \
		--eval '(setf cl-naive-log:*log-file-pathname* (truename #P"$(LOG_FILE)"))' \
		--eval '(let ((initfile #P"~/.cl-naive-webserver-test-server.lisp")) (when (probe-file initfile) (load initfile)))' \
		--eval '(cl-naive-webserver.hunchentoot.tests::run-server)' 

clean:
	-rm *~ system-index.txt
	-find . \( -name \*.[dlw]x[36][24]fsl -o -name --version.lock \) -exec rm {} +
	make -C docs clean
