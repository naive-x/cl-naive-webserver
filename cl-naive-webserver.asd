(defsystem "cl-naive-webserver"
  :description ""
  :version "2023.11.13"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:ironclad
               :split-sequence
               :cl-getx
               :cl-naive-log)
  :components ((:file "src/package")
	       (:file "src/cookies"         :depends-on ("src/package"))
               (:file "src/resource"        :depends-on ("src/package" "src/naive-webserver"))
	       (:file "src/naive-webserver" :depends-on ("src/cookies"))))
