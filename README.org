* cl-naive-web-server

A site centric web server that uses other http servers as back ends and exposes its
own flavour of what is deemed essential functionality for a webserver. Currently
hunchentoot is supported out of the box. Support for woo is a work in progress.

** Why

In then the end I would have to say that cl-naive-web-server just fits
my mental model of a webserver better.

Try out clack or weblocks or anything else before you go here. This
package is based on my very specific preferences and it will not be as
stable as those others for some time.

Some kind of reasons list:

1. To be able to swap back ends at will to use them where they are
   best but have code that is uniform.
2. You start with a site and not a server, servers are used to serve
   sites and you can run more than one server and they can use
   different backends to serve sites.

Practically this means:

1. The package implements its own sessions. This is so that session handling between
   the different http back ends is the same and authentication is
   hooked in by default. Also there is a persistent sessions extension
   in the making that will use cl-naive-store.
2. Implements its own cookies. This is so that cookie handling between
   the different http back ends is the same.
3. Implements its own dispatcher/handler infrastructure.

** Hunchentoot Implementation

Hunchentoot is used without its "dynamic page" dispatchers (static file
dispatchers are still used) , easy-handlers and sessions.

cl-naive-webserver cookies hook into hunchentoot cookies at some point.

** Woo Implementation

Woo is extended to have distpatcher/handler infrastructure, sessions and cookies.

The woo implementation needs a lot of work

- cookies need testing
- file uploads need to be implemented in a sane way
- formatting between different output specifications, not sure I need
  to do anything about this maybe libdev takes care of this for woo, I
  just see a lot of this type of thing in hunchentoot code ... so it
  must be a thing right?

** License

MIT the doc is [[file:LICENSE][here]].

** Features

cl-naive-webserver is site centric and introduces a layer between the
user and http-servers.

** Documentation

[[docs/docs.org][Documentation]] can be found in the docs folder in the repository.

** Examples
*** Simple

TBD: check it's still up-to-date

#+BEGIN_SRC common-lisp
  (defparameter *server* (make-instance 'cl-naive-webserver.hunchentoot::hunchentoot-server
					:id "test"
					:port 3333))
  (register-server *server*)

  (start-server *server*)

  (defparameter *site* (register-site
			(make-instance 'site  :url "/")))

  (setf (gethash "/example" (handlers *site*)) (lambda (script-name)
						 (declare (ignore script-name))
						 "Yeeeha she is a live and kicking!!"))
  (drakma:http-request
   "http://localhost:3333/example"
   :method :get)
#+END_SRC

Should give you

#+BEGIN_SRC common-lisp

  "Yeeeha she is a live and kicking!!"
  200
  ((:CONTENT-LENGTH . "34") (:DATE . "Fri, 02 Jul 2021 03:52:45 GMT")
			    (:SERVER . "Hunchentoot 1.3.0") (:CONNECTION . "Close")
			    (:CONTENT-TYPE . "text/html; charset=utf-8"))
  #<PURI:URI http://localhost:3333/example>
  #<FLEXI-STREAMS:FLEXI-IO-STREAM {101390E353}>
  T
  "OK"

#+END_SRC

When sessions are enabled a token is required.

#+BEGIN_SRC common-lisp

  (setf (sessions-p *site*) t)

  (drakma:http-request
   "http://localhost:3333/example"
   :method :get)
#+END_SRC

Will give you the following because no token was supplied.

#+BEGIN_SRC common-lisp

  "<html><head><title>401 Authorization Required</title></head><body><h1>Authorization Required</h1>The server could not verify that you are authorized to access the document requested.  Either you suppl...[sly-elided string of length 506]"
  401
  ((:CONTENT-LENGTH . "506") (:DATE . "Fri, 02 Jul 2021 03:56:52 GMT")
			     (:SERVER . "Hunchentoot 1.3.0") (:CONNECTION . "Close")
			     (:CONTENT-TYPE . "text/html; charset=iso-8859-1"))
  #<PURI:URI http://localhost:3333/example>
  #<FLEXI-STREAMS:FLEXI-IO-STREAM {1016820023}>
  T
  "Authorization Required"
#+END_SRC

Get a token first and then use it

#+BEGIN_SRC common-lisp
  (let ((token
	  (drakma:http-request
	   "http://localhost:3333/token"
	   :method :get)))

    (drakma:http-request
     "http://localhost:3333/example"
     :method :get
     :parameters (list (cons "auth-token" *token*))))

#+END_SRC

Now you should get something like the following

#+BEGIN_SRC common-lisp

  "Yeeeha she is a live and kicking!!"
  200
  ((:CONTENT-LENGTH . "34") (:DATE . "Fri, 02 Jul 2021 04:01:03 GMT")
			    (:SERVER . "Hunchentoot 1.3.0") (:CONNECTION . "Close")
			    (:CONTENT-TYPE . "text/html; charset=utf-8"))
  #<PURI:URI http://localhost:3333/example?auth-token=214e...41b>
  #<FLEXI-STREAMS:FLEXI-IO-STREAM {101763FAD3}>
  T
  "OK"

#+END_SRC

** Dependencies

hunchentoot and/or woo

cl-getx, cl-who, ironclad, split-sequence, and a few cl-naive-… systems.

Check the asd files.

** Supported CL Implementations

Should support all compliant implementations, no implementation specific code was used.

It has been tested in =sbcl= and in =ccl=.

** Tests

The tests may be run from the =Makefile= or from the REPL.
There is a test server that publishes a few sites and resources,
and naive-test test-suites.

#+begin_src shell :results verbatim
make help
#+end_src

#+RESULTS:
: make documentation    # Generate documentation.
: make test             # Run the server and tests.
: make test-alone       # Run just the tests, assuming make server is running.
: make test-ccl         # Run the server and tests on ccl.
: make test-sbcl        # Run the server and tests on sbcl.
: make server           # Launches a test server.
: make server-ccl       # Launches a test server on ccl.
: make server-sbcl      # Launches a test server on sbcl.
: make clean            # Clean temporary files.



=make test= runs the server in a parallel process, and runs the test suites (=test-sbcl=).


Alternatively, we may:

=make server= runs the server (=server-sbcl=).  It leaves the lisp
REPL available to the programmer for debugging or live-patching.

For example, one may ask to enter the debugger upon error and change
the minimal log level, by evaluating at the REPL:

#+begin_src common-lisp
(setf cl-naive-tests:*debug* t
      cl-naive-webserver:*debug* cl-naive-tests:*debug*
      cl-naive-webserver:*log-minimal-level* :debug)
#+end_src

The server runs in a separate thread (and may fork threads for each
requests), so be prepared to switch thread to debug them when they
break into the debugger (exact commands depending on the CL
implementation).

and:

=make test-alone= just runs the test suites, assuming that the server is already running.


Finally, we may just run the server and/or the tests in a CL REPL.

To load and run the test server:

#+begin_src common-lisp
  (defparameter cl-user::*dependency-dir* #P"~/path/to/dir/where/cl-naive-stuff/is/cloned/")
  ;; of (setf (uiop:getenv "DEPENDENCYDIR") "/absolute/path/to/dir/where/cl-naive-stuff/is/cloned/")
  (load "tests/hunchentoot/run-server.lisp")
#+end_src

and, in the same REPL, or a separate REPL, run the test:

#+begin_src common-lisp
  (ql:quickload :cl-naive-webserver.hunchentoot.tests)
  (cl-naive-tests:run)
  (cl-naive-tests:report)
#+end_src
