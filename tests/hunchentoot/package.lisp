(in-package :common-lisp-user)

(defpackage :cl-naive-webserver.hunchentoot.tests
  (:use :cl :cl-naive-webserver :cl-naive-tests)
  (:shadowing-import-from :cl-naive-tests :*debug*)
  (:export :run-server :*port* :*start-own-server* :*server-log-file*))
