(in-package :cl-naive-webserver)

#-(and) ;; we should have some independent testcases around here.
    (testcase :handler
              :test-func (lambda (testcase info)
                           (declare (ignore info))
                           (equalp
                            (not (not (getf testcase :actual)))
                            (getf testcase :expected)))
              :expected t
              :actual (find-resource *site* "/example")
              :info "Checking if handler exists.")
