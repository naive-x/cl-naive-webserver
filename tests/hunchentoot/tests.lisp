(in-package :cl-naive-webserver.hunchentoot.tests)

(defvar *start-own-server* t)

(defvar *port* 33001)

(defvar *cookie-jar* (make-instance 'drakma:cookie-jar)) ; browser side saved cookies.
(defvar *token* nil) ; browser side saved token.

(defvar *server-process* nil)
(defvar *server-log-stream* nil)
(defvar *server-log-file*)

(eval-when (:compile-toplevel :execute)
  (defparameter *dependency-dir*
    (let ((sysdir (asdf:component-pathname
                   (asdf:find-system :cl-naive-webserver.hunchentoot.tests))))
      (truename
       (make-pathname :directory (butlast (pathname-directory sysdir))
                      :name nil :type nil :version nil
                      :host (pathname-host sysdir) :device (pathname-device sysdir))))))

(defun setup ()
  (let* ((*dependency-dir*  (or (uiop:getenv "DEPENDENCYDIR") *dependency-dir*))
         (*server-log-file* (cl-naive-log:log-file-pathname :info nil))
         (sbcl-server-command
          `("sbcl" "--dynamic-space-size" "4096"
                   "--noinform" "--no-userinit"
                   ;; "--eval" "(push :use-ansi-colors *features*)"
                   "--eval" "(load #P\"~/quicklisp/setup.lisp\")"
                   "--eval" ,(format nil "(push ~S ql:*local-project-directories*)" *dependency-dir*)
                   "--eval" "(push #P\"./\" asdf:*central-registry*)"
                   "--eval" "(ql:quickload :cl-naive-webserver.hunchentoot.tests.server)"
                   "--eval" "(in-package :CL-NAIVE-WEBSERVER.HUNCHENTOOT.TESTS)"
                   "--eval" ,(format nil "(setf *port* ~A)" *port*)
                   "--eval" "(setf cl-naive-log:*minimal-log-level* :debug)"
                   "--eval" ,(format nil "(setf cl-naive-log:*log-file-pathname* ~S)" *server-log-file*)
                   "--eval" "(setf cl-naive-webserver:*report-backtraces* nil)"
                   ;; "--eval" "(let ((initfile #P\"~/.cl-naive-webserver-test-server.lisp\")) (when (probe-file initfile) (load initfile)))"
                   "--eval" "(cl-naive-webserver.hunchentoot.tests::run-server)")))
    (if *start-own-server*
      (setf *server-process*
            (multiple-value-list
             (uiop:launch-program sbcl-server-command
                                  :input :stream
                                  :output nil ; :stream
                                  :error nil)))
      (progn
        (format t "~%Please launch a sbcl process with the following command:~%~{~S~^ ~}~%"
                sbcl-server-command)
        (format t "Press RET to continue: ")
        (finish-output)
        (read-line)))
    (sleep 3)
    ;; (setf *server-log-stream* (open *log-file* :if-does-not-exist :create
    ;;                                            :if-exists :append
    ;;                                            :direction :io
    ;;                                            ;; :element-type 'character :external-format :utf-8
    ;;                                            ))
    ;; (file-position *server-log-stream* :end)
    ;; (print *server-process*)
    ;; (format *trace-output* "Server process ~A started on port ~A~%"
    ;;         (uiop:process-info-pid *server-process*) *port*)
    (finish-output *trace-output*)
    (values)))

(defun slurp (process)
  (let ((stream (uiop:process-info-output process)))
    (loop while (listen stream)
          collect (read-line stream nil nil))))

(defun teardown ()
  (when *start-own-server*
    (when *server-process*
      ;; (print (slurp (first *server-process*)))
      (write-line "(quit)" (uiop:process-info-input (car *server-process*)))
      (finish-output (uiop:process-info-input (car *server-process*)))
      (sleep 2)
      (uiop:terminate-process (car *server-process*))
      ;; (format *trace-output* "Server process ~A terminated~%"
      ;;         (uiop:process-info-pid *server-process*))
      (finish-output *trace-output*)
      (setf *server-process* nil))
    (when *server-log-stream*
      (close *server-log-stream*)
      (setf *server-log-stream* nil)))
  (values))


(defmacro with-test-environment (&body body)
  `(handler-bind ((USOCKET:ADDRESS-IN-USE-ERROR
                    (lambda (condition)
                      (cl-naive-webserver::report-error condition)
                      nil))
                  (error
                    (lambda (condition)
                      (cl-naive-webserver::report-error condition)
                      nil)))
     (unwind-protect
          (progn
            (setup)
            (progn
              ,@body))
       (teardown))))

(defmacro without-blocking (&body body)
  `(handler-case
       (bt:with-timeout (5) ,@body)
     ((or bt:timeout usocket:timeout-error)
       ()
       :timeout)))

(defmacro with-error-handling (&body body)
  `(handler-case
       (progn ,@body)
     (error (err)
       (princ err *trace-output*)
       (terpri *trace-output*)
       500)))

(testsuite  :webserver-no-session
            ;; Doing tests without token first, because token gets set in session
            ;; there are no sessions by default
            (with-test-environment
              (testcase :success-example
                        :expected 200
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/basic/example" *port*)
                                         :method :get
                                         :connection-timeout 10)
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      code))

                        :info "Fetch url no session.")

              (testcase :server-error
                        :expected 500
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/basic/error" *port*)
                                         :method :get
                                         :connection-timeout 10)
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      code))

                        :info "Fetch url no session.")

              (testcase :example-cookie
                        :expected "yummy=cookie"
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/basic/example-cookie" *port*)
                                         :method :get
                                         :connection-timeout 10)
                                      (declare (ignorable output code))
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      (cdr (find :set-cookie header :key #'car))))
                        :info "Url sets cookie.")))

(testsuite :webserver-session
           ;; Doing tests with token needed.
           (with-test-environment
             (testcase :example-fail-fetch-no-session
                       :expected 401
                       :actual (with-error-handling
                                   (multiple-value-bind (output code header)
                                       (drakma:http-request
                                        (format nil "http://localhost:~A/session/example" *port*)
                                        :method :get
                                        :connection-timeout 10)
                                     (declare (ignorable output header))
                                     (when *debug*
                                       (print (list code header output) *trace-output*)
                                       (terpri *trace-output*))
                                     code))

                       :info "Fail fetch no session.")
             (testcase :token
                       :expected 128
                       :actual (with-error-handling
                                   (multiple-value-bind (output code header)
                                       (drakma:http-request
                                        (format nil "http://localhost:~A/session/token" *port*)
                                        :method :get)
                                     (declare (ignorable code header))
                                     (setf *token* output)
                                     (when *debug*
                                       (print (list code header output) *trace-output*)
                                       (terpri *trace-output*))
                                     (length output)))
                       :info "Get token.")
             (testcase :example-token-as-get-param
                       :expected 200
                       :actual (with-error-handling
                                   (multiple-value-bind (output code header)
                                       (drakma:http-request
                                        (format nil "http://localhost:~A/session/example" *port*)
                                        :method :get
                                        :parameters (list (cons "auth-token" *token*)))
                                     (declare (ignorable output header))
                                     (when *debug*
                                       (print (list code header output) *trace-output*)
                                       (terpri *trace-output*))
                                     code))

                       :info "Use token to fetch page in get.")
             (testcase :example-token-as-post-param
                       :expected 200
                       :actual (with-error-handling
                                   (multiple-value-bind (output code header)
                                       (drakma:http-request
                                        (format nil "http://localhost:~A/session/example" *port*)
                                        :method :post
                                        :parameters (list (cons "auth-token" *token*)))
                                     (declare (ignorable output header))
                                     (when *debug*
                                       (print (list code header output) *trace-output*)
                                       (terpri *trace-output*))
                                     code))

                       :info "Use token to fetch page in post.")
             (testcase :example-token-cookie
                       :expected 200
                       :actual (with-error-handling
                                   (multiple-value-bind (output code header)
                                       (drakma:http-request
                                        (format nil "http://localhost:~A/session/token" *port*)
                                        :method :get
                                        :cookie-jar *cookie-jar*)
                                     (declare (ignorable output header))
                                     (when *debug*
                                       (print (list code header output) *trace-output*)
                                       (terpri *trace-output*))
                                     code))

                       :info "Get token and put token received in cookie into jar.")
             (testcase :token-cookie
                       :expected 200
                       :actual (with-error-handling
                                   (multiple-value-bind (output code header)
                                       (drakma:http-request
                                        (format nil "http://localhost:~A/session/example" *port*)
                                        :method :get
                                        :cookie-jar *cookie-jar*)
                                     (declare (ignorable output header))
                                     (when *debug*
                                       (print (list code header output) *trace-output*)
                                       (terpri *trace-output*))
                                     code))

                       :info "Use cookie jar to fetch page.")))

(testsuite  :webserver-credentials
            (with-test-environment
              (testcase :example
                        :expected 401
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/authed/example" *port*)
                                         :method :get)
                                      (declare (ignorable output header))
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      code))

                        :info "Fetch url without token or credentials.")

              (testcase :token-fail-auth
                        :expected 401
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/authed/token" *port*)
                                         :method :post
                                         :parameters (list (cons "user-name" "Piet")
                                                           (cons "password" "eish")))
                                      (declare (ignorable output header))
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      code))

                        :info "Fetch token with bad credentials.")

              (testcase :token-pass-auth
                        :expected 128
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/authed/token" *port*)
                                         :method :post
                                         :parameters (list (cons "user-name" "Piet")
                                                           (cons "password" "Snot")))
                                      (declare (ignorable code header))
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      (setf *token* output)
                                      (length output)))
                        :info "Fetch token with good credentials")

              (testcase :example-token-as-get-param
                        :expected 200
                        :actual (with-error-handling
                                    (multiple-value-bind (output code header)
                                        (drakma:http-request
                                         (format nil "http://localhost:~A/authed/example" *port*)
                                         :method :get
                                         :parameters (list (cons "auth-token" *token*)))
                                      (declare (ignorable output header))
                                      (when *debug*
                                        (print (list code header output) *trace-output*)
                                        (terpri *trace-output*))
                                      code))

                        :info "Use token fetched with credentials.")))
