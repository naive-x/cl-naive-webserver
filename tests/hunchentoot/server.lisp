(in-package :cl-naive-webserver.hunchentoot.tests)

(defvar *port* 33001)

(defvar *server* nil)
(defvar *token* nil)
(defvar *cookie-jar* nil)
(defvar *site* nil)
(defvar *site-with-credentials* nil)
(defvar *site-with-sessions* nil)


(eval-when (:compile-toplevel)
  (defvar *source-dir* (make-pathname :name nil :type nil :version nil
				      :defaults *compile-file-truename*)))

(defmethod authenticate ((credentials credentials-basic) &key &allow-other-keys)
  (if (and (equalp (user-name credentials) "Piet")
           (equalp (password credentials) "Snot"))
      (make-instance 'authorization
                     :credentials credentials)))

(defgeneric get-urls (site)
  (:method ((site site))
    (let ((url (url site))
          (urls '()))
      (maphash (lambda (path handler)
                 (declare (ignore handler))
                 (push (format nil "http://localhost:~A~A~A" *port* url path) urls))
               (handlers site))
      urls)))

(defun get-cookie (name)
  (cdr (assoc name (hunchentoot:cookies-in*) :test (function equalp))))

(defun serve-an-error ()
  (/ 3 0))


(defun setup ()
  (setf *server* (make-instance 'cl-naive-webserver.hunchentoot::hunchentoot-server
                                :id "test"
                                :port *port*))

  (register-server *server*)
  (start-server *server*)

  ;; -------------------- 

  (setf *site* (register-site (make-instance 'site  :url "/")))

  ;; test a static file thru exclusion-acceptor-mixin
  (setf (find-resource (handlers *site*) "/favicon.ico")
	#.(make-pathname :defaults *source-dir* :name "favicon" :type "ico"))

  ;; -------------------- 

  (flet ((create-site (url &key sessions-p credentials-class)
	   (let ((site (register-site (make-instance 'site  
                                                     :url url
                                                     :credentials-class credentials-class))))
             (setf (sessions-p site) sessions-p)

             (setf (find-resource (handlers site) "/dump")
                   (lambda (script-name)
                     (setf (cl-naive-webserver:return-code cl-naive-webserver:*reply*) 200)
                     (push (cons :content-type "text/plain") (cl-naive-webserver:headers cl-naive-webserver:*reply*))
	             (format nil "script-name  = ~S~%cookie yummy = ~S~%~{~20A = ~S~%~}"
                             script-name
                             (get-cookie "yummy")
                             (list :headers-in      (hunchentoot:headers-in      hunchentoot:*request*)
                                   :request-method  (hunchentoot:request-method  hunchentoot:*request*)
                                   :request-uri     (hunchentoot:request-uri     hunchentoot:*request*)
                                   :server-protocol (hunchentoot:server-protocol hunchentoot:*request*)
                                   :local-addr      (hunchentoot:local-addr      hunchentoot:*request*)
                                   :local-port      (hunchentoot:local-port      hunchentoot:*request*)
                                   :remote-addr     (hunchentoot:remote-addr     hunchentoot:*request*)
                                   :remote-port     (hunchentoot:remote-port     hunchentoot:*request*)
                                   :content-stream  (hunchentoot::content-stream hunchentoot:*request*)
                                   :post-data       (when (string-equal (hunchentoot::header-in :content-type hunchentoot:*request*)
                                                                        "multipart/form-data")
                                                      (hunchentoot::get-post-data))
                                   :cookies-in      (hunchentoot:cookies-in      hunchentoot:*request*)
                                   :get-parameters  (hunchentoot:get-parameters  hunchentoot:*request*)
                                   :post-parameters (hunchentoot:post-parameters hunchentoot:*request*)
                                   :script-name     (hunchentoot:script-name     hunchentoot:*request*)
                                   :query-string    (hunchentoot:query-string    hunchentoot:*request*)
                                   :session         (hunchentoot:session         hunchentoot:*request*)
                                   :aux-data        (hunchentoot::aux-data       hunchentoot:*request*)))))
             
             (setf (find-resource (handlers site) "/example")
                   (lambda (script-name)
                     (setf (cl-naive-webserver:return-code cl-naive-webserver:*reply*) 200)
                     (push (cons :content-type "text/plain") (cl-naive-webserver:headers cl-naive-webserver:*reply*))
	             (format nil "Yeeeha she is a live and kicking!~2%script-name  = ~S~%cookie yummy = ~S~2%"
                             script-name
                             (get-cookie "yummy"))))

             (setf (find-resource (handlers site) "/error")
                   (lambda (script-name)
                     (declare (ignore script-name))
                     (serve-an-error)))

             (setf (find-resource (handlers site) "/example-cookie")
                   (lambda (script-name)
                     (setf (cl-naive-webserver:return-code cl-naive-webserver:*reply*) 200)
                     (push (cons :content-type "text/plain") (cl-naive-webserver:headers cl-naive-webserver:*reply*))
                     (set-cookie "yummy" :value "cookie")
                     (format nil "Yeeeha she is a live and kicking!~2%script-name  = ~S~%cookie yummy = ~S~%Check headers for cookie.~2%"
                             script-name
                             (get-cookie "yummy"))))
             site)))

    (setf *site*                   (create-site "/basic"))
    (setf *site-with-sessions*     (create-site "/session" :sessions-p t))
    (setf *site-with-credentials*  (create-site "/authed"  :sessions-p t :credentials-class 'credentials-basic))) 

  (finish-output *standard-output*)
  (format *trace-output* "~&DID SET UP~%")
  (finish-output *trace-output*)
  *server*)

(defun teardown ()
  (finish-output *standard-output*)
  (format *trace-output* "~&WILL TEARDOWN~%")
  (finish-output *trace-output*)
  (when *server*
    (stop-server *server*)
    (when *site-with-credentials* (deregister-site *site-with-credentials*))
    (when *site-with-sessions*     (deregister-site *site-with-sessions*))
    (when *site*                  (deregister-site *site*))
    (deregister-server *server*)
    (setf *server* nil
          *site* nil
          *site-with-sessions* nil
          *site-with-credentials* nil)
    (sleep 1)))

(defmacro with-test-environment (&body body)
  `(handler-bind ((USOCKET:ADDRESS-IN-USE-ERROR
                    (lambda (condition)
                      (cl-naive-webserver::report-error condition)
                      nil))
                  (error
                    (lambda (condition)
                      (cl-naive-webserver::report-error condition)
                      nil)))
     (unwind-protect
          (progn (setup)
                 ,@body)
       (teardown))))


(defun list-server-urls ()
  (format t "~&Urls:")
  (map nil 'print (append (get-urls *site*)
                          (get-urls *site-with-sessions*)
                          (get-urls *site-with-credentials*)))
  (terpri))

(defun list-server-sites ()
  (format t "~&Sites:~%")
  (maphash (lambda (k v)
             (declare (ignore v))
             (format t "  ~S~%" k))
           cl-naive-webserver::*sites*))


(defvar *quit-server* nil)
(defvar *setup-teardown-thread* nil)
(defun quit ()
  (when *setup-teardown-thread*
    (setf *quit-server* t)
    (bt:join-thread *setup-teardown-thread*)
    (setf *quit-server* nil
          *setup-teardown-thread* nil))
  #+ccl (ccl:quit)
  #+sbcl (sb-ext:quit))

#+(or ccl sbcl) (shadowing-import 'quit "CL-USER")

#+sbcl
(let ((counter 0))
  (defun enclosed-prompt (stream)
    (format stream "~2%S/~A[~D]> "
            (if (packagep *package*)
                (first (sort (cons (package-name *package*)
                                   (copy-list (package-nicknames *package*)))
                             (function <=) :key (function length)))
                "#<INVALID *PACKAGE*>")
            (incf counter))))

#+sbcl
(defun prompt (stream)
  (enclosed-prompt stream))

#+sbcl
(setf sb-int:*repl-prompt-fun* (function prompt))

(defun run-server ()
  "Starts the test server from a separate thread, and wait for *quit-server* to tear it down."
  (setf *package* (find-package :cl-naive-webserver.hunchentoot.tests))
  (setf *setup-teardown-thread*
        (bt:make-thread
         (lambda ()
           (with-test-environment
               (format t "Welcome to the cl-naive-webserver test server.~2%")
             (format t "Interesting commands: ~{~A~^ ~}~%" '((list-server-urls) (list-server-sites) (quit)))
             (loop :do (sleep 1)
                   :until *quit-server*)))
         :name "server-setup-teardown")))



(defun server-loop ()
  (with-test-environment
      (loop
         (princ "server> " *query-io*)
         (finish-output *query-io*)
         (let ((command (read-line *query-io*)))
           (when (string-equal "quit" (string-trim command))
             (return-from server-loop))))))
