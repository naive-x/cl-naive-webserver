(defpackage :cl-naive-webserver.tests.client
  (:use
   :common-lisp
   :drakma)
  (:export :run-post-examples))

(in-package :cl-naive-webserver.tests.client)

(defun post (url headers parameters content-type contents)
  (drakma:http-request url
                       :method :post
                       :additional-headers headers
                       :parameters parameters
                       :content-type content-type
                       :content contents))


(defun run-post-examples ()
  
  (print (multiple-value-list
          (post "http://localhost:33001/basic/dump"
                nil
                '(("Poo-1" . "Bar")
                  ("Poo-2" . "Quux"))
                "text/plain"
                "Hello world!")))

  (print (multiple-value-list
          (post "http://localhost:33001/basic/dump"
                '(("Hoo-1" . "Bar")
                  ("Hoo-2" . "Quux"))
                '(("Poo-1" . "Bar")
                  ("Poo-2" . "Quux"))
                nil nil))))
