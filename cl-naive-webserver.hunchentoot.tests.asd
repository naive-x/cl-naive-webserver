(defsystem "cl-naive-webserver.hunchentoot.tests"
  :description "Tests for cl-naive-webserver.hunchentoot."
  :version "2023.4.18"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:usocket :cl-naive-webserver.hunchentoot :cl-naive-tests
                        :drakma)
  :components ((:file "tests/usocket-patches")
               (:file "tests/hunchentoot/package")
               (:file "tests/hunchentoot/tests" :depends-on ("tests/hunchentoot/package"))))

